import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import { body, validationResult } from 'express-validator';

import { MessagesService } from '../shared/services/messages.service';

const router = express.Router();

router.post(
    '/',
    body('message').exists().isString().isLength({ min: 1 }),
    async (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req);

        // In bigger scale apps this will be exported to a middleware
        if (!errors.isEmpty()) {
            return res.status(400).json({
                success: false,
                errors: errors.array(),
            });
        }

        try {
            const msgId = await MessagesService.handleNewMessage(
                req.body.message,
            );
            res.send(msgId);
        } catch (e) {
            next(e);
        }
    },
);

export { router as messagesRouter };
