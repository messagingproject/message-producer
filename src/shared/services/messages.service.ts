import { v4 as uuidv4 } from 'uuid';
import { KafkaService } from './kafka.service';

class MessagesService {
    static handleNewMessage = (msg: string) => {
        const msgId = uuidv4();
        KafkaService.produceMessage(msg, msgId);

        return msgId;
    };
}

export { MessagesService };
