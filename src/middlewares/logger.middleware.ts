import * as expressWinston from 'express-winston';
import * as winston from 'winston';

const loggerMiddleware = expressWinston.logger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(winston.format.json()),
    meta: true,
    msg: 'HTTP {{req.method}} {{req.url}}',
    expressFormat: true,
    ignoreRoute: function (req, res) {
        return false;
    },
});

export { loggerMiddleware };
