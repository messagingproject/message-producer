import * as expressWinston from 'express-winston';
import * as winston from 'winston';

const errorLoggerMiddleware = expressWinston.errorLogger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(winston.format.json()),
});

export { errorLoggerMiddleware };
