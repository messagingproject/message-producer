import { NextFunction, Request, Response } from 'express';

import { GeneralError } from '../shared/errors';

const errorMiddleware = (
    err: GeneralError,
    req: Request,
    res: Response,
    next: NextFunction,
) => {
    res.locals.message = err.message;
    res.status(err.status || 500).end();
};

export { errorMiddleware };
