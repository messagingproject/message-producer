import * as dotenv from 'dotenv';
import * as express from 'express';
import { Request, Response } from 'express';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import { Server as ioServer } from 'socket.io';
import * as http from 'http';

dotenv.config();

import {
    errorMiddleware,
    loggerMiddleware,
    errorLoggerMiddleware,
    notFoundMiddleware,
} from './middlewares';
import { messagesRouter } from './routers';
import { KafkaService } from './shared/services';

// This is a business decision!
// In case the connection to kafka doesn't work - the service will NOT be available
KafkaService.initKafka();

const app = express();

// Logging & request parsing middlewares
app.use(cors());
app.use(loggerMiddleware);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Routes
app.get('/health', (req: Request, res: Response) =>
    res.send('Server is up and running'),
);
app.use('/messages', messagesRouter);

// error middlewares
app.use(notFoundMiddleware);
app.use(errorLoggerMiddleware);
app.use(errorMiddleware);

const server = http.createServer(app);
server.listen(process.env.PORT);

// io socket
const io = new ioServer(server, { cors: { origin: '*' } });
KafkaService.consumeStatuses((status, key) =>
    io.emit('new_status', { status, key }),
);
